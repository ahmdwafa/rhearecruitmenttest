﻿using System.Threading.Tasks;

namespace AccountManager
{
    public interface IAccountService
    {
        Task<double> GetAccountAmount(int accountId);
    }
}
