﻿using AccountManager;
using AccountManagerTests.Mocks;
using NUnit.Framework;
using System.Threading.Tasks;

namespace AccountManagerTests
{
    public class AccountInfoTests
    {
        [Test]
        public void RefreshAmount_WhenNotCalledAfterInitialization_AmountShouldBeZero()
        {
            // Arrange
            int accountId = 10;

            var mockAccountService = new MockAccountService();

            var accountInfo = new AccountInfo(accountId, mockAccountService);

            // Assert
            Assert.Zero(accountInfo.Amount);
        }

        [Test]
        public async Task RefreshAmount_WhenCalledAfterInitialization_AmountIsSameAsThatOfIAccountService()
        {
            // Arrange
            int accountId = 10;

            var mockAccountService = new MockAccountService();

            var accountInfo = new AccountInfo(accountId, mockAccountService);

            // Act
            await accountInfo.RefreshAmount();

            // Assert
            Assert.AreEqual((await mockAccountService.GetAccountAmount(accountId)), accountInfo.Amount);
        }

        [Test]
        public async Task RefreshAmount_WhenCalledAfterAccountIdChanges_AmountIsRefreshed()
        {
            // Arrange
            int accountId = 10;

            var mockAccountService = new MockAccountService();

            var accountInfo = new AccountInfo(accountId, mockAccountService);
            await accountInfo.RefreshAmount();

            double oldAmount = accountInfo.Amount;

            // Act
            accountId = 20;
            accountInfo = new AccountInfo(accountId, mockAccountService);
            await accountInfo.RefreshAmount();

            // Assert
            Assert.AreNotEqual(oldAmount, accountInfo.Amount);
        }
    }
}
