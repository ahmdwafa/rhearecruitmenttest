﻿using AccountManager;
using System.Threading.Tasks;

namespace AccountManagerTests.Mocks
{
    public class MockAccountService : IAccountService
    {
        public async Task<double> GetAccountAmount(int accountId)
        {
            await Task.Delay(100);

            return accountId * 1000;
        }
    }
}
